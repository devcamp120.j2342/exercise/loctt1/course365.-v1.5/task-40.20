$(document).ready(function(){
//REGION 1
var gIdCourse = 0;
var gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
const gCOURSE_COLS = [
    'id',
    'courseCode',
    'courseName',
    'price',
    'duration',
    'level',
    'teacherName',
    'action',
];
var gCourseTable = $('#table-course').DataTable({
    columns: [
        { data: gCOURSE_COLS[0] },
        { data: gCOURSE_COLS[1] },
        { data: gCOURSE_COLS[2] },
        { data: gCOURSE_COLS[3] },
        { data: gCOURSE_COLS[4] },
        { data: gCOURSE_COLS[5] },
        { data: gCOURSE_COLS[6] },
        { data: gCOURSE_COLS[7] },
    ],
    columnDefs: [
        {
            // định nghĩa lại cột action
            targets: 7,
            className: 'd-flex',
            defaultContent: `
    <button class = "btn btn-success edit-course" id="btn-sua"><i class="fas fa-pencil-alt"></i></button>
    <button class = "btn btn-danger ml-2 delete-course" id="btn-xoa"><i class="fas fa-trash-alt"></i></button>
`,
        },
    ],
});
//REGION 2
onPageLoading();
//gán sự kiện nút thêm course
$("#btn-add").on('click', function(){
    onBtnAddClick()
});
//gán sự kiện nút xác nhận thêm khóa học trên modal
$("#id-confirm-add").on('click', function(){
    onBtnAddConfirmCourse();
})
//gán sự kiện nút sửa
$("#table-course").on('click', ".edit-course", function(){
    onBtnEditClick(this);
});
//gán sự kiện nút xác nhận sửa trên modal
$("#btn-confirm-edit").on('click', function(){
    onBtnEditConfirmClick();
})
//gán sự kiện nút xóa
$("#table-course").on('click', '.delete-course', function(){
    onBtnDeleteClick(this);
})
//gán sự kiện nút xác nhận xóa
$("#btn-confirm-xoa").on("click", function(){
    onBtnDeleteCourseHandleClick()
})
//REGION 3
function onPageLoading(){
    getAllCourse();
} 
//hàm thự hiện sự kiện nút thêm click
function onBtnAddClick(){
    $("#modal-add-course").modal("show");
}
//hàm thực hiện chức năng update trên modal
function onBtnEditConfirmClick(){
    // khai báo đối tượng chứa data
		var vCourseObj = {
			courseCode: '',
			courseName: '',
			price: '',
			discountPrice: '',
			duration: '',
			level: '',
			coverImage: '',
			teacherName: '',
			teacherPhoto: '',
			isPopular: '',
			isTrending: '',
		};
        //b1 thu thập dữ liệu
        getUpdateCourseData(vCourseObj);
        //b2 kiểm tra dữ liệu
        //b3 gọi api
        $.ajax({
            url: gBASE_URL + '/courses/' + gIdCourse,
            type: 'PUT',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(vCourseObj),
            success: function (paramRes) {
                // B4: xử lý front-end
                handleUpdateCourseSuccess();
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            },
        });
}
//hàm sử lý hiển thị nút update
function handleUpdateCourseSuccess() {
    alert('Sửa course thành công!');
    getAllCourse();
    resertUpdateCourseForm();
    $('#modal-edit-course').modal('hide');
}
//
function resertUpdateCourseForm(){
    $('#inp-courseCode').val('');
    $('#inp-courseName').val('');
    $('#inp-price').val('');
    $('#inp-discountprice').val('');
    $('#inp-duration').val('');
    $('#select-level').val('');
    $('#inp-cover-img').val('');
    $('#inp-teachername').val('');
    $('#inp-teacher-photo').val('');
    $('#select-popular').val('');
    $('#select-trending').val('');
}
//hàm thực hiện chức năng thêm course
function onBtnAddConfirmCourse(){
    //khai báo đối tượng lưu trữ dữ liệu
    var vAddCourseObj = {
        courseCode: "",
        courseName: "",
        price: "",
        discountPrice: "",
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: "",
    }
    //b1 thu thập dữ liệu
    getDataAddCourseModal(vAddCourseObj);
    //b2 kiểm tra dữ liệu
    var vIscheckCourseAdd = validateCourseObj(vAddCourseObj);
    if(vIscheckCourseAdd){
        //b3 gọi api
        $.ajax({
            url: gBASE_URL + '/courses',
            type: "POST",
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(vAddCourseObj),
            success: function(paramRes){
                console.log(paramRes);
                //b4 hiển thị
                headleInsertCourseSuccess(paramRes);
            },
            error: function(paramErr){
                console.log(paramErr.status);
            }
        })
        
    }
}
//hàm thực hiện sự kiện nút sửa click
function onBtnEditClick(paramEdit){
    // lưu thông tin course id đang được edit vào biến toàn cục
    gIdCourse = getCourseIdFromButton(paramEdit);
    callApiGetIdByCourse(gIdCourse);
};
//hàm thực hiện sự kiện nút delete được ấn
function onBtnDeleteClick(paramCourse){
    gIdCourse = getCourseIdFromButton(paramCourse)
    $("#modal-delete-course").modal('show');
}
//hàm thực hiện sự kiện chức năng xóa
function onBtnDeleteCourseHandleClick() {
    // khai báo đối tượng chứa voucher data
    // B1: Thu thập dữ liệu
    // B2: Validate insert
    // B3: insert voucher
    $.ajax({
        url: gBASE_URL + '/courses/' + gIdCourse,
        type: 'DELETE',
        contentType: 'application/json;charset=UTF-8',
        success: function (paramRes) {
            // B4: xử lý front-end
            handleDeleteUserSuccess();
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        },
    });
}
//REGION 4
//hàm thực hiện chức năng dalete
function handleDeleteUserSuccess() {
    alert('xóa course thành công!');
    getAllCourse();
    $('#modal-delete-course').modal('hide');
}
//hàm thu thập dữ update trên modal
function getUpdateCourseData(paramUserObj) {
    paramUserObj.courseCode = $('#inp-courseCode').val().trim();
    paramUserObj.courseName = $('#inp-courseName').val().trim();
    paramUserObj.price = $('#inp-price').val().trim();
    paramUserObj.discountPrice = $('#inp-discountprice').val().trim();
    paramUserObj.duration = $('#inp-duration').val().trim();
    paramUserObj.level = $('#select-level').val();
    paramUserObj.coverImage = $('#inp-cover-img').val().trim();
    paramUserObj.teacherName = $('#inp-teachername').val().trim();
    paramUserObj.teacherPhoto = $('#inp-teacher-photo').val().trim();
    paramUserObj.isPopular = $('#select-popular').val();
    paramUserObj.isTrending = $('#select-trending').val();
}
//hàm gọi api để lấy id course
function callApiGetIdByCourse(paramIdCourse){
    $.ajax({
        url: gBASE_URL + '/courses/' + paramIdCourse,
        type: 'GET',
        success: function (paramCourse) {
            showCourseDataToModal(paramCourse);
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        },
    });
    //hiển thị modal lên
    $("#modal-edit-course").modal("show");
}
//hàm load dữ liệu vào các trường modal sửa
function showCourseDataToModal(paramCourse) {
    $('#inp-courseCode').val(paramCourse.courseCode);
    $('#inp-courseName').val(paramCourse.courseName);
    $('#inp-price').val(paramCourse.price);
    $('#inp-discountprice').val(paramCourse.discountPrice);
    $('#inp-duration').val(paramCourse.duration);
    $('#select-level').val(paramCourse.level);
    $('#inp-cover-img').val(paramCourse.coverImage);
    $('#inp-teachername').val(paramCourse.teacherName);
    $('#inp-teacher-photo').val(paramCourse.teacherPhoto);
    $('#select-popular').val(paramCourse.isPopular);
    $('#select-trending').val(paramCourse.isTrending);
    console.log(paramCourse.isTrending);
}
//hàm thu thập dữ liệu add course trên modal
function getDataAddCourseModal(paramObj){
    paramObj.courseCode = $("#inp-coursecode").val().trim();
    paramObj.courseName = $("#inp-coursename").val().trim();
    paramObj.price = $("#inp-price").val().trim();
    paramObj.discountPrice = $("#inp-discountprice").val().trim();
    paramObj.duration = $("#inp-duration").val().trim();
    paramObj.level = $("#inp-level").val();
    paramObj.coverImage = $("#inp-cover-img").val().trim();
    paramObj.teacherName = $("#inp-teachername").val().trim();
    paramObj.teacherPhoto = $("#inp-teacherphoto").val().trim();
    paramObj.isPopular = $("#select-popular").val().trim();
    paramObj.isTrending = $("#select-trending").val().trim();
}
//hàm kiểm tra dữ liệu nhập vào trên form thêm khóa học trên modal
function validateCourseObj(paramUserObj){
    if (paramUserObj.courseCode === '') {
        alert('courseCode cần nhập');
        return false;
    }
    if (paramUserObj.courseName === '') {
        alert('courseName cần nhập');
        return false;
    }
    if (paramUserObj.price === '') {
        alert('price cần nhập');
        return false;
    }
    if (paramUserObj.discountPrice === '') {
        alert('discountPrice cần nhập');
        return false;
    }
    if (paramUserObj.duration === '') {
        alert('duration cần nhập');
        return false;
    }
    if (paramUserObj.level === '') {
        alert('level cần chọn');
        return false;
    }
    if (paramUserObj.coverImage === '') {
        alert('coverImage cần nhập');
        return false;
    }
    if (paramUserObj.teacherName === '') {
        alert('teacherName cần nhập');
        return false;
    }
    if (paramUserObj.teacherPhoto === '') {
        alert('teacherPhoto cần nhập');
        return false;
    }
    if (paramUserObj.isPopular === '') {
        alert('isPopular cần chọn');
        return false;
    }
    if (paramUserObj.isTrending === '') {
        alert('isTrending cần chọn');
        return false;
    }

    return true;
}
//hàm hiển thị thông báo thêm khóa học thành công
function headleInsertCourseSuccess(){
    $("#modal-add-course").modal('hide') // ẩn modal xuống
    $("#modal-add-ok").modal("show"); // hiển thị modal thông báo đã thêm khóa học thành công
    getAllCourse() // load lại dataTable
    resertFormCourseAdd() // làm mới lại form thêm khóa học ở modal
}
//hàm làm trắng form thêm kháo học ở modal thêm
function resertFormCourseAdd(){
    $("#inp-coursecode").val('');
    $("#inp-coursename").val('');
    $("#inp-price").val('');
    $("#inp-discountprice").val('');
    $("#inp-duration").val('');
    $("#inp-level").val('');
    $("#inp-cover-img").val('');
    $("#inp-teachername").val('');
    $("#inp-teacherphoto").val('');
    $("#select-popular").val('0');
    $("#select-trending").val('0')
}
//hàm gọi api lấy danh sách khóa học
function getAllCourse(){
    $.ajax({
        url: gBASE_URL + "/courses",
        type: "GET",
        success: function(response){
            console.log(response);
            loadDataToCourseTable(response)
        },
        error: function(err){
            console.log(err.status);
        }
    })
};
//lấy id kháo học thông qua nút button
function getCourseIdFromButton(paramButton) {
    var vTableRow = $(paramButton).parents('tr');
    var vCourseRowData = gCourseTable.row(vTableRow).data();
    return vCourseRowData.id;
}
//hàm load dữ liệu vào table
function loadDataToCourseTable(paramResponse){
    gCourseTable.clear()
    gCourseTable.rows.add(paramResponse);
    gCourseTable.draw();
}
});